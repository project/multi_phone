<?php

namespace Drupal\multi_phone\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\AliasManager;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'MultiPhoneBlock' block.
 *
 * @Block(
 *  id = "multi_phone_block",
 *  admin_label = @Translation("Multi phone block"),
 * )
 */
class MultiPhoneBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  private $pathMatcher;

  /**
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $currentPath;

  /**
   * @var \Drupal\Core\Path\AliasManager
   */
  private $aliasManager;

  /**
   * MultiPhoneBlock constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   * @param \Drupal\Core\Path\AliasManager $alias_manager
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    PathMatcherInterface $path_matcher,
    CurrentPathStack $current_path,
    AliasManager $alias_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory;
    $this->pathMatcher = $path_matcher;
    $this->currentPath = $current_path;
    $this->aliasManager = $alias_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('path.matcher'),
      $container->get('path.current'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#cache' => [
        'contexts' => ['url.path'],
      ]
    ];
    $config = $this->config->get('multi_phone.blockconfig');
    $lists = $config->get('list');
    $current_path = $this->currentPath->getPath();
    $current_alias = $this->aliasManager->getAliasByPath($current_path);

    if (is_array($lists)) {
      foreach ($lists as $list) {
        if ($this->pathMatcher->matchPath($current_alias, $list['path'])) {
          $clean_phone = preg_replace('/[^\d+]/', '', $list['number']);
          $url  = Url::fromUri("tel:" . substr($clean_phone, 0, 13));

          $title = [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $this->t('@phone', ['@phone' => $list['number']]),
            '#attributes' => ['class' => ['multi-phone-block__text']],
          ];

          $build = [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['multi-phone-block']
            ],
            '#cache' => [
              'contexts' => ['url.path'],
            ],
          ];
          $build[] = [
            '#title' => $title,
            '#type' => 'link',
            '#url' => $url,
            '#attributes' => ['class' => ['multi-phone-block__link']],
          ];
          break;
        }
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts()
  {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
