<?php

namespace Drupal\multi_phone\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BlockConfig.
 */
class BlockConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'multi_phone.blockconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multi_phone_block_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('multi_phone.blockconfig');
    $values = $config->get('list') ?: [];

    // Override config values with form_state values in case of ajax callback.
    if ($form_state->get('ajax_callback')) {
      $values = $form_state->getValue('list') ?: $values;
    }

    // Add an empty field for add one callback.
    if ($form_state->get('ajax_callback_add_one')) {
      $values[] = [
        'number' => '',
        'path' => '',
        'weight' => ''
      ];
    }

    $form['desc'] = [
      '#type' => 'container',
    ];

    $form['desc'][] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t("Enter a phone number in the 'Number' field and assign a path you wish for that number to be displayed on in the 'Path' field."),
    ];

    $form['desc'][] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t("Paths must begin with a '/'. Paths can also use the wildcard represented by a '*'"),
    ];

    $form['numbers'] = [
      '#type' => 'container',
      '#title' => $this->t('Account Settings'),
      '#group' => 'social',
      '#description' => $this->t('hello')
    ];

    $form['numbers']['list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Number'),
        $this->t('Path'),
        $this->t('Remove'),
      ],
      '#tree' => TRUE,
      '#prefix' => '<div id="numbers-wrapper">',
      '#suffix' => '</div>',
      '#description' => $this->t('hello'),
    ];

    foreach ($values as $delta => $field) {
      $form['numbers']['list'][$delta] = [
        '#attributes' => ['class' => ['draggable']],
        'number' => [
          '#type' => 'textfield',
          '#default_value' => $field['number'] ?: '',
          '#weight' => 0,
          '#size' => 20,
        ],
        'path' => [
          '#type' => 'textarea',
          '#default_value' => $field['path'] ?: '',
          '#weight' => 1,
          '#size' => 30,
          '#cols' => 5,
          '#rows' => 2,
        ],
        'remove' => [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#name' => 'remove_' . $delta,
          '#submit' => [[$this, 'removeFieldsubmit']],
          '#ajax' => [
            'callback' => [$this, 'ajaxCallback'],
            'wrapper' => 'numbers-wrapper',
            'effect' => 'fade',
          ],
          '#weight' => 2,
        ],
      ];
    }

    $form['numbers']['add'] = [
      '#type' => 'submit',
      '#name' => 'addfield',
      '#value' => $this->t('Add another number'),
      '#submit' => [[$this, 'addfieldsubmit']],
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'numbers-wrapper',
        'effect' => 'fade',
      ],
    ];

    $form_state->set('ajax_callback', FALSE);
    $form_state->set('ajax_callback_add_one', FALSE);
    return parent::buildForm($form, $form_state);
  }



  /**
   * {@inheritdoc}
   */
  public function addfieldsubmit(array &$form, FormStateInterface &$form_state) {
    $form_state->set('ajax_callback', TRUE);
    $form_state->set('ajax_callback_add_one', TRUE);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Remove field on submit.
   */
  public function removeFieldsubmit(array &$form, FormStateInterface &$form_state) {
    $form_state->set('ajax_callback', TRUE);
    $trigger = $form_state->getTriggeringElement();
    $delta = $trigger['#parents'][1];
    $values = $form_state->getValues();
    unset($values['list'][$delta]);
    $form_state->setValue('list', $values['list']);
    unset($form['numbers']['list'][$delta]);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax submit callback.
   */
  public function ajaxCallback(array &$form, FormStateInterface &$form_state) {
    return $form['numbers']['list'];
  }

  /**
   * Remove empty values on submit.
   */
  public function cleanValues(&$values) {
    foreach ($values['list'] as $delta => $number) {
      if (empty($number['number']) || empty($number['path'])) {
        unset($values['list'][$delta]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('multi_phone.blockconfig')
      ->set('list', $form_state->getValue('list'))
      ->save();
  }

}
